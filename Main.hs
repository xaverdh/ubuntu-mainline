{-# language LambdaCase, OverloadedStrings #-}
module Main where

import UbuntuMainline.Package
import UbuntuMainline.Operations

import Data.Semigroup ((<>))

import Control.Applicative
import Control.Monad

import Options.Applicative.Common
import Options.Applicative.Extra
import Options.Applicative.Builder


defSigKey = "kernelppa.gpg"

verOption :: Parser Version
verOption = option
  ( eitherReader readVersion )
  ( short 'v' <> long "version" <> help versionHelp )
  where
    versionHelp =
      "The kernel version e.g. \"--version 4.11.3\"."

verArgument :: Parser Version
verArgument = argument
  ( eitherReader readVersion )
  ( metavar "KERNEL_VERSION" <> help versionHelp )
  where
    versionHelp =
      "The kernel version e.g. \"4.11.3\"."


flavOption :: Parser Flavor
flavOption = option
  ( eitherReader readFlavor )
  ( short 'f' <> long "flavor"
  <> value Generic <> showDefaultWith showFlavor
  <> help flavorHelp )
  where
    flavorHelp =
      "The kernel flavor e.g. \"--flavor lowlatency\"."

archOption :: Parser Arch
archOption = option
  ( eitherReader readRealArch )
  ( short 'a' <> long "arch"
  <> value Amd64 <> showDefaultWith showArch
  <> help archHelp )
  where
    archHelp =
      "The kernel architecture e.g. \"--arch i386\"."


keyOption :: Parser FilePath
keyOption = strOption
  ( short 'k' <> long "sigkey"
  <> value defSigKey <> showDefault <> help sigKeyHelp )
  where
    sigKeyHelp =
      "The public key file used to verify the\
      \ signature of the checksums file."

localSwitch :: Parser OperationMode
localSwitch = flag Remote Local
  ( short 'l' <> long "local"
  <> showDefaultWith showOperationMode
  <> help localHelp )
  where
    localHelp =
      "Do not download, assume packages are already\
      \ present in the current working directory."


noCleanupSwitch :: Parser CleanupPolicy
noCleanupSwitch = flag Clean NoClean
  ( short 'n' <> long "no-cleanup"
  <> showDefaultWith showCleanupPolicy
  <> help noCleanupHelp )
  where
    noCleanupHelp =
      "Do not clean up packages after operation."

withArgs :: Parser (IO a) -> IO a
withArgs progParser =
  join $ customExecParser
    ( prefs $ showHelpOnEmpty <> showHelpOnError )
    ( addHelper progParser `info` desc )
  where
    desc = fullDesc <> header "Install mainline kernels."

main :: IO ()
main = withArgs $ subparser
    (  command "install" (addHelper install `info` installDesc)
    <> command "remove" (addHelper remove `info` removeDesc)
    <> command "fetch" (addHelper fetch `info` fetchDesc)
    <> command "verify" (addHelper verify `info` verifyDesc)
    <> command "cleanup" (addHelper cleanup `info` cleanupDesc) )
  where
    install = appKernelSpec
      ( installKernel <$> localSwitch
      <*> noCleanupSwitch <*> keyOption )
    remove = appKernelSpec $ pure removeKernel
    fetch = appKernelSpec $ pure fetchKernel 
    verify = verifyKernel <$> keyOption
    cleanup = appKernelSpec $ pure cleanupKernel
    appKernelSpec f = f
      <*> flavOption <*> archOption <*> verArgument

installDesc = progDesc "Install mainline kernel."

removeDesc = progDesc "Remove mainline kernel."

fetchDesc = progDesc "Fetch kernel packages\
            \ (but do not verify or install)."

verifyDesc = progDesc "Verify already downloaded\
            \ kernel packages."

cleanupDesc = progDesc "Clean up leftover package\
            \ files from given kernel."


addHelper = (helper <*>)


