{-# language LambdaCase #-}
module UbuntuMainline.Verify
  ( verify
  , verifyGpg
  , verifyHashes )
where

import UbuntuMainline.Utils

import System.Exit
import System.IO
import System.Process
import System.Directory

verify :: FilePath -> IO ()
verify pubkey = do
  verifyGpg pubkey signatureFile hashFile
  verifyHashes hashFile
  where
    hashFile = "CHECKSUMS"
    signatureFile = "CHECKSUMS.gpg"

verifyGpg :: FilePath -> FilePath -> FilePath -> IO ()
verifyGpg pubkey sigfile file = do
  keyring <- makeAbsolute pubkey
  callProcessExitK "/usr/bin/gpg" 
    [ "--no-default-keyring"
    , "--keyring" , keyring
    , "--verify" , sigfile , file ] "" $ \exCode -> 
    case exCode of
      ExitSuccess -> putStrLn "Gpg signature of checksums verified."
      _ -> dieWith exCode "Gpg signature validation failed!"

verifyHashes :: FilePath -> IO ()
verifyHashes hashFile =
  callProcessExitK "/usr/bin/sha256sum"
    [ "--ignore-missing"
    , "-c", hashFile ] "" $ \exCode -> case exCode of
      ExitSuccess -> putStrLn "Checksums verified."
      _ -> dieWith exCode "Checksum validation failed!"


