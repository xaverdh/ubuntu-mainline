{-# language OverloadedStrings, LambdaCase #-}
module UbuntuMainline.Network
  ( getRemoteRepoHeader
  , serverUrl
  , repoUrl 
  , download )
where

import UbuntuMainline.Package
import UbuntuMainline.Utils

import System.Exit
import System.FilePath
import System.IO

import Control.Concurrent
import Control.Concurrent.Chan
import Control.Lens
import Control.Monad
import Control.Monad.Extra
import Data.Semigroup ((<>))
import Data.Maybe

import Network.Wreq

import qualified Data.Text as T
import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Encoding as LEnc
import qualified Data.ByteString.Lazy as LB

type Url = String
type Spec = (Url,FilePath)

safeGet :: Url -> IO (Either Int LB.ByteString)
safeGet url = do
  resp <- get url
  let st = resp ^. responseStatus . statusCode
  pure $ if st /= 200
    then Left st
    else Right $ resp ^. responseBody

serverUrl :: Url
serverUrl = "http://kernel.ubuntu.com/~kernel-ppa/mainline"

repoUrl :: Version -> Url
repoUrl ver = serverUrl
  </> ("v" <> T.unpack (dottedHuman ver) )

textBody :: LB.ByteString -> T.Text
textBody = LT.toStrict . LEnc.decodeUtf8

getRemoteRepoHeader :: Version -> IO T.Text
getRemoteRepoHeader ver =
  eitherM
    (\i -> dieWith (ExitFailure i) "http failure")
    (pure . LT.toStrict . LEnc.decodeUtf8)
    $ safeGet (repoUrl ver </> "HEADER.html")

download :: [(Url,FilePath)] -> IO ()
download specs = do
  chan <- newChan
  -- Spawn worker threads:
  forM_ specs (forkIO . downloadWorker chan)
  -- Wait for spawned threads to finish:
  results <- take (length specs) <$> getChanContents chan
  -- Error handling:
  unless (all isJust results) $ dieWithError "Http failure."

downloadWorker :: Chan (Maybe Spec) -> Spec -> IO ()
downloadWorker chan (spec@(url,filepath)) = do
  res <- safeGet url
  case res of
    Left st -> do
      logErr $ "Failed to download " <> fmt spec
        <> "\nhttp code was: " <> show st
      writeChan chan Nothing
    Right bs -> do
      LB.writeFile filepath bs
      logMsg $ "Sucessfully downloaded " <> fmt spec
      writeChan chan $ Just spec


fmt :: Spec -> String
fmt (url,path) = "\"" <> path <> "\" from \"" <> url <> "\""



