{-# language LambdaCase #-}
module UbuntuMainline.Utils
  ( dieWith
  , dieWithError 
  , callProcessAssertSuccess
  , callProcessExitK
  , readProcessK
  , debug
  , logMsg
  , logErr )
where

import Data.Semigroup ((<>))
import System.Exit
import System.IO
import System.Process


dieWith :: ExitCode -> String -> IO a
dieWith exCode err = do
  logErr err
  exitWith exCode

dieWithError :: String -> IO a
dieWithError = dieWith (ExitFailure 1)

callProcessAssertSuccess :: FilePath -> [String] -> IO ()
callProcessAssertSuccess proc args = do
  readProcessK proc args "" $ \exCode _ _ -> case exCode of
    ExitSuccess -> pure ()
    ExitFailure i -> dieWith exCode $ "Child Process \""
      <> proc <> "\" failed with exit code " <> show i <> "."

callProcessExitK :: FilePath -> [String] -> String 
  -> (ExitCode -> IO a) -> IO a
callProcessExitK proc args input k = 
  readProcessK proc args input $ \exCode _ _ -> k exCode

readProcessK :: FilePath -> [String] -> String
  -> (ExitCode -> String -> String -> IO a) -> IO a
readProcessK proc args input k = do
  (exCode,out,err) <- readProcessWithExitCode proc args input
  k exCode out err

-- Debugging:

logMsg :: String -> IO ()
logMsg
  | debug = hPutStrLn stdout
  | True = const $ pure ()

logErr :: String -> IO ()
logErr
  | debug = hPutStrLn stderr 
  | True = const $ pure ()


debug = False




