{-# language OverloadedStrings, LambdaCase #-}
module UbuntuMainline.Package
  ( Package(..)
  , PackType(..)
  , Version(..)
  , Release(..)
  , Flavor(..)
  , Arch(..)
  , headers
  , image
  , dotted
  , dottedHuman
  , padded
  , archMap
  , readVersion
  , showFlavor
  , readFlavor
  , showArch
  , readArch
  , readRealArch
  , compPackName )
where

import Text.Regex.Applicative
import System.Directory (listDirectory,getCurrentDirectory)
import Data.Semigroup hiding (Any,All)
import Data.Either.Extra
import Control.Monad
import Numeric.Natural
import qualified Data.Text as T
import qualified Data.Map as M
import qualified Data.List as L
import qualified Data.Char as C
import Data.Tuple (swap)

data Package = Package PackType Version Flavor Arch
  deriving (Eq,Ord)

data PackType = Headers | Image
  deriving (Eq,Ord)

data Version = Version Natural Natural Release
  deriving (Eq,Ord)

data Release = Regular Natural | Candidate Natural
  deriving (Eq,Ord)

data Flavor = Any | Generic | LowLatency
  deriving (Eq,Ord)

data Arch = All | Amd64 | Arm64 | Armhf | I386 | Pcc64el | S390x
  deriving (Eq,Ord)

archMap = M.fromList
  [ "all" --> All
  , "amd64" --> Amd64
  , "arm64" --> Arm64
  , "armhf" --> Armhf
  , "i386" --> I386
  , "pcc64el" --> Pcc64el
  , "s390x" --> S390x ]
  where a --> b = (a,b)


headers = Package Headers
image = Package Image

dotted :: Version -> T.Text
dotted (Version n k rel) =
  T.intercalate "." (showT <$> [n,k,j])
  where
    j = case rel of
      Regular i -> i
      Candidate i -> 0

dottedHuman :: Version -> T.Text
dottedHuman (Version n k rel) =
  showT n <> "." <> showT k <> rest
  where
    rest = case rel of
      Regular 0 -> ""
      Regular i -> "." <> showT i
      Candidate i -> "-rc" <> showT i

padded :: Version -> T.Text
padded (Version n k rel) =
  padShow n <> padShow k <> rest
  where
    padShow = T.justifyRight 2 '0' . showT
    rest = case rel of
      Regular i -> padShow i
      Candidate i -> padShow 0 <> "rc" <> showT i

readVersion :: String -> Either String Version
readVersion s = maybeToEither err (s =~ verRegex)
  where
    num = read <$> some (psym C.isDigit)
    verRegex = Version
      <$> num <*> (sym '.' *> num)
      <*> releaseRegex 
    releaseRegex =
      ( fmap Candidate (string "-rc" *> num )
      <|> fmap Regular (sym '.' *> num)
      <|> pure (Regular 0) )
    err = "Invalid version format.\
          \  Expected \"?.?[.?][-rc?]\", got " ++ show s

showFlavor :: Flavor -> String
showFlavor = \case
  Generic -> "generic"
  LowLatency -> "lowlatency"

readFlavor :: String -> Either String Flavor
readFlavor s = maybeToEither err $ M.lookup s flavMap
  where
   err = "Unknown flavor: \"" <> s
          <> "\" expected one of: "
          <> L.intercalate "," (M.keys flavMap)
   flavMap = M.fromList
     [ "generic" --> Generic
     , "lowlatency" --> LowLatency ]
   a --> b = (a,b)

showArch :: Arch -> String
showArch = \case
  All -> "all"
  Amd64 -> "amd64"
  Arm64 -> "arm64"
  Armhf -> "armhf"
  I386 -> "i386"
  Pcc64el -> "pcc64el"
  S390x -> "s390x"


readRealArch :: String -> Either String Arch
readRealArch s = maybeToEither err $ M.lookup s archMap'
  where
    archMap' = M.delete "all" archMap
    err = "Unknown architecture: \"" <> s
       <> "\" expected one of: "
       <> L.intercalate "," (M.keys archMap)


readArch :: String -> Either String Arch
readArch s = maybeToEither err $ M.lookup s archMap
  where
   err = "Unknown architecture: \"" <> s
          <> "\" expected one of: "
          <> L.intercalate "," (M.keys archMap)


compPackName :: Package -> T.Text
compPackName (Package typ ver flav _) =
  T.concat
    [ case typ of
      { Headers -> "linux-headers-";
        Image -> "linux-image-" }
    , (dotted ver <> "-") , padded ver
    , case flav of
      { Any -> "";
        Generic -> "-generic";
        LowLatency -> "-lowlatency" } ]

-- utils

showT = T.pack . show



