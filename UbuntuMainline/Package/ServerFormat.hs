{-# language OverloadedStrings #-}
module UbuntuMainline.Package.ServerFormat
  ( getPackageFileNamesRemotely
  , getPackageFileNamesLocally
  , universalPackageRegex )
where

import UbuntuMainline.Package
import UbuntuMainline.Network
import UbuntuMainline.Utils

import System.Directory

import Control.Monad
import Data.Functor
import Data.Semigroup ((<>))
import Data.Maybe

import Text.Regex.Applicative.Text
import qualified Data.Char as C
import qualified Data.Set as S
import qualified Data.Text as T
import qualified Data.Map as M

getPackageFileNamesLocally :: Version
  -> Flavor -> Arch -> IO (Maybe [String])
getPackageFileNamesLocally ver flav arch = do
  cwd <- getCurrentDirectory
  content <- T.pack . join <$> listDirectory cwd
  -- ^ Todo: rewrite extractNames, so we dont have to throw
  --         away structured information here by joining
  pure $ extractNames content ver Generic Amd64

getPackageFileNamesRemotely :: Version -> Flavor -> Arch -> IO [String]
getPackageFileNamesRemotely ver flav arch = do
  t <- getRemoteRepoHeader ver
  case extractNames t ver flav arch of
    Nothing -> dieWithError "Wrong number of package matches\
        \ on server side. Perhaps the website changed?"
    Just pks -> pure pks


compRegex :: Package -> RE Char T.Text
compRegex ( package@(Package typ ver flav arch)) =
  foldr (liftA2 (<>)) (pure "") regexes
  where
    num = T.pack <$> some (psym C.isDigit)
    regexes =
      [ string $ compPackName package
      , string ("_" <> dotted ver <> "-")
      , string $ padded ver
      , string "." , num , string "_"
      , string $ T.pack $ showArch arch
      , string ".deb" ]

universalPackageRegex :: RE Char (Maybe Package)
universalPackageRegex =
  genPack <$> packType <*> (packVerDot <* sym '.') <*> packVerPad
  <*> (packFlavor <* sym '_') <*> (packVerDot <* sym '-')
  <*> (packVerPad <* sym '.' <* num <* sym '.')
  <*> (packArch <* string ".deb")
  where
    choice = foldr (<|>) empty
    num = read <$> some (psym C.isDigit)
    num2 = (\c1 c2 -> read [c1,c2])
      <$> psym C.isDigit <*> psym C.isDigit
    packType = 
      ( string "linux-headers-" $> Headers
      <|> string "linux-image-" $> Image )
    packVerDot = 
      Version <$> (num <* sym '.') <*> (num <* sym '.')
        <*> fmap Regular num
    packVerPad = 
      Version <$> num2 <*> num2 
        <*> ( fmap Candidate (num2 *> string "rc" *> num)
            <|> fmap Regular num2 ) 
    packFlavor =
      ( string "-generic" $> Generic
      <|> string "-lowlatency" $> LowLatency
      <|> pure Any )
    packArch = choice
      $ map (\(s,r) -> string (T.pack s) $> r)
      $ M.toList archMap
    genPack :: PackType -> Version -> Version -> Flavor
            -> Version -> Version -> Arch -> Maybe Package
    genPack typ v1 v2 flav v3 v4 arch
      | all (==v1) [v2,v3,v4] = Just $ Package typ v1 flav arch
      | True = Nothing


extractNames :: T.Text
  -> Version -> Flavor -> Arch
  -> Maybe [String]
extractNames txt ver flav arch =
  fmap T.unpack <$> extractNamesT txt ver flav arch

extractNamesT :: T.Text
   -> Version -> Flavor -> Arch
   -> Maybe [T.Text]
extractNamesT txt ver flav arch =
  sanitize =<< fmap makeUnique (txt =~ regex) where
  regex = catMaybes <$> many
    ( optional (compRegex $ headers ver Any All)
    <|> optional (compRegex $ headers ver flav arch)
    <|> optional (compRegex $ image ver flav arch)
    <|> (anySym $> Nothing) )
  
makeUnique :: Ord a => [a] -> [a]
makeUnique = S.toList . S.fromList 

sanitize :: [a] -> Maybe [a]
sanitize xs = case xs of
  [_,_,_] -> Just xs
  _ -> Nothing


