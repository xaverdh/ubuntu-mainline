{-# language LambdaCase #-}
module UbuntuMainline.Operations
  ( OperationMode(..)
  , CleanupPolicy(..)
  , showOperationMode
  , showCleanupPolicy
  , installKernel
  , removeKernel
  , fetchKernel
  , verifyKernel
  , cleanupKernel )
where

import Control.Monad.Privilege

import UbuntuMainline.Package
import UbuntuMainline.Package.ServerFormat
import UbuntuMainline.Network
import UbuntuMainline.Verify
import UbuntuMainline.Utils

import Data.Semigroup ((<>))
import qualified Data.Text as T

import Control.Monad
import System.Process
import System.FilePath
import System.Directory
import System.IO
import System.Exit


hashFile = "CHECKSUMS"
signatureFile = "CHECKSUMS.gpg"

withPolicy :: (UserPolicy -> GroupPolicy -> a) -> a
withPolicy run = run SudoUser SudoUser 

fetchKernel :: Flavor -> Arch -> Version -> IO ()
fetchKernel flav arch ver = do
  withPolicy runUnPrivileged [0,1,2] . unpriv' $ do
    fetch flav arch ver

cleanupKernel :: Flavor -> Arch -> Version -> IO ()
cleanupKernel flav arch ver =
  withPolicy runUnPrivileged [0,1,2] . unpriv' $ do
    getPackageFileNamesLocally ver flav arch >>= \case
      Nothing -> putStrLn "Nothing to clean up." 
      Just pks -> do
        cleanUp (hashFile : signatureFile : pks)
        putStrLn "Clean up successful."

verifyKernel :: FilePath -> IO ()
verifyKernel pubkey =
  withPolicy runUnPrivileged [0,1,2] . unpriv' $ do
    verify pubkey

removeKernel :: Flavor -> Arch -> Version -> IO ()
removeKernel flav arch ver =
  runPrivileged . priv $ do
    callProcess "/usr/bin/apt" ( "remove" : pks )
  where
    pks = map (T.unpack . compPackName)
      [ Package Image ver flav arch
      , Package Headers ver flav arch
      , Package Headers ver Any All ]


data OperationMode = Local | Remote

showOperationMode :: OperationMode -> String
showOperationMode = \case
  Local -> "local"
  Remote -> "remote"

data CleanupPolicy = Clean | NoClean

showCleanupPolicy :: CleanupPolicy -> String
showCleanupPolicy = \case
  Clean -> "clean up"
  NoClean -> "no not clean up"

installKernel :: OperationMode -> CleanupPolicy
  -> FilePath -> Flavor -> Arch -> Version -> IO ()
installKernel local clean pubkey flav arch ver =
  withPolicy runPriv $ do
    case local of
      Local -> pure () 
      Remote -> do
        priv $ putStrLn "Downloading kernel.."
        exCode <- (if debug then unprivWithFds [1,2] else unpriv)
                  $ fetch flav arch ver
        priv $ case exCode of
          ExitSuccess -> do
            putStrLn "..done"
          ExitFailure i -> do
            hPutStr stderr "Failed to fetch kernel!"
            exitWith exCode
    unprivWithFds' [1,2] $ verify pubkey
    priv $ installKernelLocally
    unprivWithFds' [1,2] $ case clean of
      Clean ->
        getPackageFileNamesLocally ver flav arch >>= \case
          Nothing -> pure ()
          Just pks -> cleanUp (hashFile:signatureFile:pks)
      NoClean -> pure ()
  where
    localNotFound = "Trying to install locally,\
          \ but not all packages could be found\
	  \ in the current working directory!"

    installKernelLocally :: IO ()
    installKernelLocally = do
      pks <- getPackageFileNamesLocally ver flav arch
             >>= maybe (dieWithError localNotFound) pure
      install pks


{- helpers -}

fetch :: Flavor -> Arch -> Version -> IO ()
fetch flav arch ver = do
  pks <- getPackageFileNamesRemotely ver flav arch
  let names = hashFile : signatureFile : pks
  let urls = fmap (repoUrl ver </>) names
  download $ zip urls names

install files = void $ callProcess "/usr/bin/sudo"
  ( "/usr/bin/dpkg" : "-i" : files )

cleanUp = mapM_ removeFile





